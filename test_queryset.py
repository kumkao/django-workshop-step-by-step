import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproj.settings")
django.setup()

from blog.models import Post, STATUS, Q

# SELECT * FROM blog_post WHERE id = 1;
post = Post.objects.get(title='post 1')

# SELECT * FROM blog_post WHERE status = 1;
posts = Post.objects.filter(status=STATUS.PUBLISHED)

# SELECT * FROM blog_post WHERE status in (0, 1);
posts = Post.objects.filter(title__startswith='tile')
posts = Post.objects.filter(content__contains='OS') \
        .filter(status=STATUS.PUBLISHED) \

# SELECT * FROM blog_post
# WHERE (status = 0 AND author = 'test')
# OR (status = 1 AND content LIKE '%content');
condition1 = Q(status=0, author='test')
condition2 = Q(status=1, content__endswith='content')
posts = Post.objects.filter(condition1 & condition2)