from django.db import models
from django.core.validators import MinLengthValidator, RegexValidator
from tinymce import models as tinymce_models


class STATUS:
  DRAFT = 0
  DELETED = -1
  PUBLISHED = 1


STATUS_CHOICES = (
  (STATUS.DELETED, 'deleted', ),
  (STATUS.DRAFT, 'draft', ),
  (STATUS.PUBLISHED, 'published', ),
)


class Post(models.Model):

  title = models.CharField(
    max_length=256,
    blank=False,
    null=False,
    validators=[
      MinLengthValidator(2),
      RegexValidator(r'[a-zA-Z]+')
    ]
  )

  author = models.CharField(
    max_length=256,
    blank=False,
    null=True,
    default=None,
  )

  content = tinymce_models.HTMLField(
    blank=True,
    default='',
    null=False,
  )

  status = models.SmallIntegerField(
    choices=STATUS_CHOICES,
    default=STATUS.DRAFT,
  )

  created_at = models.DateTimeField(
    auto_now_add=True,
    editable=False,
  )

  def __str__(self):
    return '{} ({})'.format(self.title, self.get_status_display())

  def __unicode__(self):
    return u'{} ({})'.format(self.title, self.get_status_display())


class Comment(models.Model):

  post = models.ForeignKey(
    Post,
    null=False,
    on_delete=models.CASCADE,
  )

  display_name = models.CharField(
    max_length=256,
    blank=False,
    null=False
  )

  content = models.TextField(
    blank=False,
    null=False,
  )

  created = models.DateTimeField(
    auto_now_add=True,
    editable=False,
    verbose_name="วันที่สร้าง"
  )

  last_updated = models.DateTimeField(
    auto_now=True,
    editable=False,
    verbose_name="วันที่แก้ไขล่าสุด"
  )

  def __str__(self):
    return '{0}:{1}'.format(self.display_name, self.content[:30])

  def __unicode__(self):

    # str1 = 'abcdefg'
    # str1[0]
    # str1[1:]
    # str1[:5]
    # str1[2:-2]
    return u'{0}:{1}'.format(self.display_name, self.content[:30])

