from rest_framework import viewsets
from .serializers import PostSerializer
from .models import Post
from django_filters import FilterSet


class PostFilter(FilterSet):

  class Meta:
    model = Post
    fields = {
      'status': ['exact'],
      'title': ['contains'],
      'content': ['contains'],
      'created_at': ['gte', 'lte']
    }


class PostViewSet(viewsets.ModelViewSet):

  queryset = Post.objects.all()
  serializer_class = PostSerializer

  filterset_class = PostFilter