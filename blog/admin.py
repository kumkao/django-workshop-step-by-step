from django.contrib import admin
from .models import Post, Comment, STATUS
from django.utils.html import format_html


class CommentInlineAdmin(admin.StackedInline):

  model = Comment
  extra = 1


STATUS_COLOR = {
  STATUS.DELETED: 'red',
  STATUS.DRAFT: 'grey',
  STATUS.PUBLISHED: 'green',
}


class PostAdmin(admin.ModelAdmin):

  list_display = ['title', 'author', 'content', 'created_at', 'custom_status']
  search_fields = ['title', 'author', 'content', 'comment__content']
  list_filter = ['status', 'created_at']
  inlines = [CommentInlineAdmin]

  def custom_status(self, obj):
    status_text = obj.get_status_display()
    status_color = STATUS_COLOR[obj.status]

    return format_html('''
      <div style="text-align: center; color: {};">
        <strong>{}</strong>
      </div>
    ''',status_color, status_text)

  custom_status.empty_value_display = 'unknown'
  custom_status.admin_order_field = 'status'
  custom_status.short_description = 'Current Status'


class CommentAdmin(admin.ModelAdmin):

  list_display = ['post', 'display_name', 'content', 'created']


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
