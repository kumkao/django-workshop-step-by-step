from rest_framework import serializers
from .models import Post, Comment


class PostSerializer(serializers.ModelSerializer):

  title = serializers.CharField()
  # author = serializers.CharField()
  # content = serializers.CharField()
  # status = serializers.IntegerField()
  created_at = serializers.DateTimeField()

  status_text = serializers.SerializerMethodField()

  def get_status_text(self, obj):

    return obj.get_status_display()

  class Meta:

    model = Post
    fields = ['id', 'title', 'created_at', 'status_text']
