import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproj.settings")
django.setup()


# code live here
from blog.models import Post, Comment, STATUS


# create new post object
post = Post(
  title='post 1',
  content='test',
  status=STATUS.PUBLISHED
)

# save post to database
post.save()

# create a comment of created post
comment = Comment(
  post=post,
  display_name='user 01',
  content='this is a comment',
)
# save comment to database
comment.save()