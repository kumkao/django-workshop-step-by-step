Django==2.2.2
django-filter==2.1.0
django-grappelli==2.12.3
django-tinymce==2.8.0
djangorestframework==3.9.4
pytz==2019.1
sqlparse==0.3.0
